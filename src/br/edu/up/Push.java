package br.edu.up;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Push {

  private static final String FIREBASE_URL = "https://exemplo-push-3e828.firebaseio.com/";
  private static final String NOTIFICATIONS = "/notifications/";
  private static final String USER = "geucimar";

  public static void main(String[] args) {

    try {
      URL url = new URL("https://fcm.googleapis.com/fcm/send");
      /// String encoding =
      /// Base64.getEncoder().encode("test1:test1".getBytes());
      
      //Map<String, String> data = new HashMap<>();
      //data.put(key, value)
      //String rawData =  "{\"registration_ids\":[\"ABC\"]}";
      
      String rawData = "{" +
        "\"to\": \"clutwSXDulM:APA91bF2vfeFu013B5LZHUL70JeCjxThgNO1f8yGopgSwaR7TVVl33_umc9ATeWvwSHkOBpmkDgCteqkrWFM12ZVKqmYzdRwnassBagdTAYbjxXh6imWOD_D1qhZy1xmA4DjjRm9rOpt\"," +
        "\"notification\": {" +
           "\"title\": \"Push enviado da Web\", " +
           "\"body\": \"Corpo do Push enviado da Web\" " +
        "}," +
        "\"data\": { " +
           "\"titulo\": \"T�tulo dos dados\", " +
           "\"descricao\": \"Descri��o dos dados enviados...\" " +
          "}" +
        "}";
      
      
      String encodedData = URLEncoder.encode(rawData, "UTF-8" ); 

      URL u = new URL("https://fcm.googleapis.com/fcm/send");
      HttpURLConnection conn = (HttpURLConnection) u.openConnection();
      conn.setDoOutput(true);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Authorization", "key=AIzaSyDShu84WyhIEdT4PCdR6CuCnjTLxoE5EFM");
      conn.setRequestProperty("Content-Type", "application/json");
      OutputStream os = conn.getOutputStream();
      os.write(encodedData.getBytes());
      InputStream content = (InputStream) conn.getInputStream();
      BufferedReader in = new BufferedReader(new InputStreamReader(content));
      String line;
      while ((line = in.readLine()) != null) {
        System.out.println(line);
      }
          

//      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//      connection.setRequestMethod("POST");
//      connection.setDoOutput(true); 
//      connection.setRequestProperty("Content-Type", "application/json");
//      connection.setRequestProperty("Authorization", "AIzaSyDShu84WyhIEdT4PCdR6CuCnjTLxoE5EFM");
//      InputStream content = (InputStream) connection.getInputStream();
//      BufferedReader in = new BufferedReader(new InputStreamReader(content));
//      String line;
//      while ((line = in.readLine()) != null) {
//        System.out.println(line);
//      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    // Map<String, Object> data = new LinkedHashMap<>();
    //
    // data.put("firstName", "John");
    // data.put("lastName", "Doe");
    // data.put("description", "has sent you friend
    // request!");
    // data.put("read", "false");
    // data.put("imgSrc", "../../img/john.jpg");
    // data.put("date", new Date().getTime());
    // String url = FIREBASE_URL + NOTIFICATIONS + USER;
    // FirebaseUtil.writeToList(url, data);

    // try {
    // URL myURL = new
    // URL("https://fcm.googleapis.com/fcm/send");
    // HttpURLConnection myURLConnection =
    // (HttpURLConnection) myURL.openConnection();
    // String userCredentials = "username:password";
    // String basicAuth = "Basic "
    // + new
    // String(Base64.getEncoder().encode(userCredentials.getBytes()));
    // myURLConnection.setRequestProperty("Authorization",
    // basicAuth);
    // myURLConnection.setRequestMethod("POST");
    // myURLConnection.setRequestProperty("Content-Type",
    // "application/x-www-form-urlencoded");
    // myURLConnection.setRequestProperty("Content-Length",
    // "" + "teste".length());
    // myURLConnection.setRequestProperty("Content-Language",
    // "en-US");
    // myURLConnection.setUseCaches(false);
    // myURLConnection.setDoInput(true);
    // myURLConnection.setDoOutput(true);
    //
    // myURLConnection.connect();
    //
    //
    // } catch (Exception e) {
    // e.printStackTrace();
    // }

    // Obtain serverKey from Project Settings -> Cloud
    // Messaging tab
    // for "My Notification Client" project in Firebase
    // Console.

    // try {
    // FirebaseOptions options = new
    // FirebaseOptions.Builder()
    // .setServiceAccount(new
    // FileInputStream("serviceAccountCredentials.json"))
    // .setDatabaseUrl("https://servidorpush-7e9a5.firebaseio.com/")
    // .build();
    // FirebaseApp.initializeApp(options);
    // } catch (FileNotFoundException e1) {
    // e1.printStackTrace();
    // }
    //
    // String token =
    // "fUDPLyEC_Qc:APA91bGCKpcQyGKF8B_AoA34W3wb_uCfhstbBhvyI9FCEafHbBn-vqIuThey_9cR2oDb6VKBMlReQXTrNSr2gXumxKk2yS_vY38JHXAnkfhQg35xpShgBt9CNQXSekYb1ksQ315lI6xF";
    // Sender sender = new FCMSender(token);
    // Message message = new Message.Builder()
    // .collapseKey("message")
    // .timeToLive(3)
    // .delayWhileIdle(true)
    // .addData("message", "Notification from Java
    // application")
    // .build();
    //
    // // Use the same token(or registration id) that was
    // earlier
    // // used to send the message to the client directly
    // from
    // // Firebase Console's Notification tab.
    // try {
    // Result result = sender.send(message, token, 1);
    // System.out.println("Result: " + result.toString());
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
  }
}